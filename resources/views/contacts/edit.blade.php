@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Edit contact</h1>
        <form action="{{url('/contacts/' . $contact->id . '/update')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="first_name">First name</label>
                <input type="text" name="first_name" value="{{$contact->first_name}}" class="form-control" id="first_name"  placeholder="First name">
                @error('first_name')
                    <small class="form-text text-muted">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="last_name">Last name</label>
                <input type="text" name="last_name" value="{{$contact->last_name}}" class="form-control" id="last_name" placeholder="Last name">
                @error('last_name')
                    <small class="form-text text-muted">{{ $message }}</small>
                @enderror
            </div>
            <a href="{{url('/phones/' . $contact->id . '/create')}}" class="btn btn-success mb-4" role="button" aria-pressed="true">Add Phone</a>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Phones</th>
                        <th scope="col">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($phones as $phone)
                        <tr>
                            <th scope="row">{{$phone->id}}</th>
                            <td>{{$phone->number}}</td>
                            <td>
                                <div class="col-md-12">
                                    <a href="{{url('/phones/' . $phone->id . '/edit')}}" class="btn btn-primary btn-block mb-1 btn-sm" role="button" aria-pressed="true">Edit</a>
                                </div>
                                <div class="col-md-12">
                                    <a href="{{url('/phones/' . $phone->id . '/delete')}}" class="btn btn-danger btn-block btn-sm" role="button" aria-pressed="true">Delete</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
@endsection