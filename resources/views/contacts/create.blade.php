@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Add new contact</h1>
        <form action="{{url('/contacts/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="first_name">First name</label>
                <input type="text" name="first_name" value="{{old('first_name')}}" class="form-control" id="first_name"  placeholder="First name">
                @error('first_name')
                    <small class="form-text text-muted">{{ $message }}</small>
                @enderror
            </div>
            <div class="form-group">
                <label for="last_name">Last name</label>
                <input type="text" name="last_name" value="{{old('last_name')}}" class="form-control" id="last_name" placeholder="Last name">
                @error('last_name')
                    <small class="form-text text-muted">{{ $message }}</small>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
@endsection