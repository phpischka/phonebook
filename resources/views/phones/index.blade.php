@extends('layouts.app')
@section('content')
    <div class="container mt-4">
        <a href="{{url('/contacts/create')}}" class="btn btn-success mb-4" role="button" aria-pressed="true">Add Contact</a>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    <th scope="col">Phones</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($contacts as $contact)
                    <tr>
                        <th scope="row">{{$contact->id}}</th>
                        <td>{{$contact->first_name}}</td>
                        <td>{{$contact->last_name}}</td>
                        <td>
                            @if($contact->phones()->count() > 0)
                                @foreach($contact->phones()->get() as $phone)
                                    <div class="col-md-12">
                                        <p>{{$phone->number}}</p>
                                    </div> 
                                @endforeach
                            @endif
                        </td>
                        <td>
                            <div class="col-md-12">
                                <a href="{{url('/contacts/' . $contact->id . '/edit')}}" class="btn btn-primary btn-block mb-1 btn-sm" role="button" aria-pressed="true">Edit</a>
                            </div>
                            <div class="col-md-12">
                                <a href="{{url('/contacts/' . $contact->id . '/delete')}}" class="btn btn-danger btn-block btn-sm" role="button" aria-pressed="true">Delete</a>
                            </div>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection