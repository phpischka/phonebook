@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Add new Phone</h1>
        <form action="{{url('/phones/store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="contact_id" value="{{$contact_id}}">
            <div class="form-group">
                <label for="number">Number</label>
                <input type="text" name="number" value="{{old('number')}}" class="form-control" id="number"  placeholder="Number">
                @error('number')
                    <small class="form-text text-muted">{{ $message }}</small>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Save</button>
        </form>
    </div>
@endsection