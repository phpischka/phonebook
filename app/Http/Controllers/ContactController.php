<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Phone;

use Illuminate\Http\Request;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts = Contact::all();
        
        return view('contacts.index', compact('contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contacts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255'
        ]);
        
        try {
            
            $contact = new Contact();
            $contact->first_name = $request->input('first_name');
            $contact->last_name = $request->input('last_name');
            $contact->save();
            
        } catch (\Exception $e) {
            
            return back()->with('status', $e);
            
        }
        
        return redirect('/phones/' . $contact->id . '/create');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $contact = Contact::where('id', $id)->first();
        
        if (!$contact) {    
            return abort(404);
        }
        
        $phones = $contact->phones;
        
        return view('contacts.edit', compact('contact', 'phones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255'
        ]);
        
        try {
            
            $contact = Contact::where('id', $id)->first();
            $contact->first_name = $request->input('first_name');
            $contact->last_name = $request->input('last_name');
            $contact->save();
            
        } catch (\Exception $e) {
            
            return back()->with('status', $e);
            
        }
        
        return redirect()->route('contacts');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::where('id', $id)->first();
        
        if ($contact) {
          $contact->delete();
        }
        return redirect()->route('contacts');
    }
}
