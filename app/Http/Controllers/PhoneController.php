<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use Illuminate\Http\Request;

class PhoneController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($contact_id)
    {
        $contact_id = $contact_id;
        
        return view('phones.create', compact('contact_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'contact_id' => 'required|integer',
            'number' => 'required|unique:phones|string|max:15'
        ]);
        
        $contact_id = $request->input('contact_id');
        
        $number = $request->input('number');
        
        try {
            
            $phone = new Phone();
            $phone->contact_id = $contact_id;
            $phone->number = $number;
            $phone->save();
            
        } catch (\Exception $e) {
            
            return back()->with('status', $e);
            
        }
        
        return redirect('/contacts/' . $contact_id . '/edit');
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Phone  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $phone = Phone::where('id', $id)->first();
        
        return view('phones.edit', compact('phone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Phone  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'number' => 'required|unique:phones|string|max:15'
        ]);
        
        try {
            
            $phone = Phone::where('id', $id)->first();
            $phone->number = $request->input('number');
            $phone->save();
            
        } catch (\Exception $e) {
            
            return back()->with('status', $e);
            
        }
        
        $contact_id = $phone->contact->id;
        
        return redirect('/contacts/' . $contact_id . '/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Phone  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phone = Phone::where('id', $id)->first();
        
        if ($phone) {
          $phone->delete();
        }
        
        $contact_id = $phone->contact->id;
        
        return redirect('/contacts/' . $contact_id . '/edit');
    }
}
