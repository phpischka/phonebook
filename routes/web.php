<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\PhoneController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

//contacts
Route::get('/contacts', [ContactController::class, 'index'])->name('contacts');
Route::get('/contacts/create', [ContactController::class, 'create']);
Route::post('/contacts/store', [ContactController::class, 'store']);
Route::get('/contacts/{id}/edit', [ContactController::class, 'edit']);
Route::post('/contacts/{id}/update', [ContactController::class, 'update']);
Route::get('/contacts/{id}/delete', [ContactController::class, 'destroy']);

//phones
Route::get('/phones/{contact_id}/create', [PhoneController::class, 'create']);
Route::post('/phones/store', [PhoneController::class, 'store']);
Route::get('/phones/{id}/edit', [PhoneController::class, 'edit']);
Route::post('/phones/{id}/update', [PhoneController::class, 'update']);
Route::get('/phones/{id}/delete', [PhoneController::class, 'destroy']);


